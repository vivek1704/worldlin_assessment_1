# worldlin_assessment_1
## Activity performed to creating this repository and uploading file

```
git init
git add .
git commit -m "intial"
git remote add origin https://gitlab.com/vivek1704/worldlin_assessment_1.git
git push -uf origin master 
```

After that steps
- moved inside the master branch
- then cretaed CICD pipeline
- then merge it into master

***

## Name
Created a registration form into nodeJs

## Description
For creating this project, we need to follow some steps
- create npm package using npm init
- install express to create a server npm install express
- created a public folder and then into public folder need to create index.html file
- now created a registration form into index.html file
- then create app.js file into the nodeJs project

## run
- node app.js
- after running the node app.js, it will generate at link for localhost
- link is http://localhost:3000


## result
- fill name and email id
- click on submit
- you will be redirected into thankyou page
