const express = require('express');
const bodyParser = require('body-parser');

const app = express();


app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static('public'));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/index.html');
});


app.post('/register', (req, res) => {
    const { name, email } = req.body;
    res.redirect('/thankyou');
});

app.get('/thankyou', (req, res) => {
    res.send('Thank you for registering!');
});

const PORT = 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port http://localhost:${PORT}`);
});
